
public class MandelbrotCalc {

	//------------------------KLASSENVARIABLEN------------------------
	
	private int maxIter;
	private ComplexCalculator cmplx = new ComplexCalculator();
	
	//------------------------KONSTRUKTOREN------------------------
	
	public MandelbrotCalc() {
		
	}
	
	public MandelbrotCalc(int maxIter) {
		this.maxIter = maxIter;
	}
	
	//------------------------KLASSENMETHODEN------------------------
	
	public void calculate(ComplexPlane plane) {
		
		ComplexNumber zIter = new ComplexNumber(0,0);						//Startwert		
		int iter = 0;														//Anzahl Iterationen die gebraucht wurden
		
		//Berechne für jeden Pixel
		for (int i = 0; i < plane.getArrayPixelsIm(); i++) {				// i:=Zeilen, j:=Spalten
			for (int j = 0; j < plane.getArrayPixelsRe(); j++) {
				
				iter = 0;
				zIter = new ComplexNumber(0,0);
				ComplexNumber zConstant = plane.getComplexNumberOfArray(i, j);
				double betragZIter = 0;
				
				while (betragZIter <= 2 && iter <= maxIter) {
					iter++;
					zIter = cmplx.add(cmplx.multiply(zIter, zIter), zConstant);
					betragZIter = cmplx.abs(zIter);
				}
				
				//OUTSIDE
				if (betragZIter > 2) {
					plane.getComplexNumberOfArray(i, j).setMandelbrotValue(iter);
				}
				//INSIDE
				if (betragZIter <= 2) {
					plane.getComplexNumberOfArray(i, j).setMandelbrotValue(-1);
				}				
			}
		}
	}	
}
