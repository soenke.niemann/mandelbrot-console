public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Intro
		System.out.println(" ====================");
		System.out.println(" ---Mandelbrot Set---");
		System.out.println(" ====================");
		System.out.println("    Following the formula z(n+1) = z(n)² + z_c");
		System.out.println("    iterated for every pixel of the image");
		System.out.println("    with z(0) = 0 and z_c the complex Number of the pixel.");
		System.out.println();
		System.out.println("    If after n_max iterations abs(z(n_max))<2 the pixel is part of the Mandelbrot set.");
		System.out.println("    You can set n_max by running the program with a integer parameter.");
		System.out.println();
		
		int maxIter = 1000;
		if(args.length > 0){
			try{
			  	maxIter = Integer.parseInt(args[0]);
				System.out.println("n_max set to " + String.valueOf(maxIter));
			} catch (NumberFormatException e) {
				System.out.println("Please use an integer as the n_max parameter.");
			}
		}	
				
		/**SCANNER für Auflösung der komplexen Ebene
		 * Die Ebene geht von Re(-1,5 bis +0,5) und Im(-1 bis +1)
		 * Die Anzahl Zeichen für die Kantenlänge des Quadrats bestimmt die Aufösung
		 */
		int squareLength = 0;
		java.util.Scanner scanner = new java.util.Scanner(System.in);
		System.out.println("--> Please define the complex plane's resolution!");
		System.out.println("    Recommended is 40, meaning 40 console-characters per 1 unit");
		while (!scanner.hasNextInt()) {
		    scanner.next();
		}
		squareLength = scanner.nextInt();

		//Erstelle ComplexPlane
		//Resolution ist 1 Zeichen pro 1 Längeneinheit von Re/Im
		//(int resolution, boolean colour, double minRe, double maxRe, double minIm, double maxIm)		
		ComplexPlane plane = new ComplexPlane(squareLength, false, -1.5, +0.5, -1, +1);		
		
		//Initialisiere die ComplexPlane		
		plane.initPlane();
		
		//Erstelle MandelbrotCalc Objekt das die Berechnung der Zugehörigkeit übernimmt
		MandelbrotCalc mandelbrot = new MandelbrotCalc(maxIter); 	//int maxIter
		double timeTaken;
		timeTaken = System.nanoTime();		
		mandelbrot.calculate(plane);			
		timeTaken = System.nanoTime() - timeTaken;
		timeTaken = (double)(timeTaken) / 1000000000;
		
		
		//Zeichne ComplexPlane
		plane.drawPlane(false); 			//boolean drawAxis	
		
		//Printe Berechnungsdauer	
		System.out.println("Computation time: " + timeTaken);				
	}
}
