
public class ComplexNumber {
	
	//------------------------KLASSENVARIABLEN------------------------
	
	private double real;
	private double imaginary;
	private int mandelbrotIters;
	
	//------------------------KONSTRUKTOREN------------------------		

	public ComplexNumber() {
	}
	
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	//------------------------GETTER/SETTER------------------------
	
	public double getRe() {
		return real;
	}
	
	public double getIm() {
		return imaginary;
	}
	
	public void setRe(double real) {
		this.real = real;
	}
	
	public void setIm(double imaginary) {
		this.imaginary = imaginary;
	}
	
	public int getMandelbrotValue() {
		return mandelbrotIters;
	}

	public void setMandelbrotValue(int mandelbrotValue) {
		this.mandelbrotIters = mandelbrotValue;
	}
	
	//------------------------KLASSENMETHODEN------------------------
		
	
}
