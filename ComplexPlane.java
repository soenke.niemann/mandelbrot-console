
public class ComplexPlane {
	
	//------------------------KLASSENVARIABLEN------------------------
	
	private double resolution; //in Pixels per 1 Re/Im
	private boolean colour;
	private double minRe;
	private double maxRe;
	private double minIm;
	private double maxIm;
	double axisLengthRe;
	double axisLengthIm;
	int arrayPixelsRe;
	int arrayPixelsIm;
	private ComplexNumber[][] array; //[Im][Re]	
	
	//------------------------KONSTRUKTOREN------------------------		

	public ComplexPlane() {		
	}
	
	public ComplexPlane(double resolution, boolean colour, double minRe, double maxRe, double minIm, double maxIm) {
		this.resolution = resolution;
		this.colour = colour;
		this.minRe = minRe;
		this.maxRe = maxRe;
		this.minIm = minIm;
		this.maxIm = maxIm;				
	}
	
	//------------------------GETTER/SETTER------------------------
	
	public int getArrayPixelsRe() {
		return arrayPixelsRe;
	}

	public int getArrayPixelsIm() {
		return arrayPixelsIm;
	}
	
	public ComplexNumber getComplexNumberOfArray(int row, int coloumn) {
		return array[row][coloumn];
	}
		
	//------------------------KLASSENMETHODEN------------------------
	
	public void initPlane() {
		axisLengthRe = Math.abs(maxRe - minRe);
		axisLengthIm = Math.abs(maxIm - minIm);		
		arrayPixelsRe = (int)(axisLengthRe * resolution) + 1;	//Damit nicht 1 resol.Schritt vor Ende aufhört
		arrayPixelsIm = (int)(axisLengthIm * resolution) + 1;	//Damit nicht 1 resol.Schritt vor Ende aufhört
		
		array = new ComplexNumber[arrayPixelsIm][arrayPixelsRe];
		
		for (int i = 0; i < arrayPixelsIm; i++) {					// i:=Zeilen, j:=Spalten
			for (int j = 0; j < arrayPixelsRe; j++) {
				array[i][j] = new ComplexNumber(	(minRe + j*(1/resolution)), 	(maxIm - i*(1/resolution))	);				
			}
		}
	}
	
	public void drawPlane(boolean drawAxis) {
		String completeLine = "";
		String addedChar = "";	
		
		//Begrenzungsstrich darüber
		String overline = "  ";
		for (int k = 0; k < 2*arrayPixelsRe+1; k++) {
			overline += "~";
		}
		System.out.println(overline);
		
		//ZEICHNEN des eigentlichen Bildes
		for (int i = 0; i < arrayPixelsIm; i++) {					// j:=Zeilen, i:=Spalten			
			
			//Begrenzungsstrich links neben dem Bild
			completeLine += " | ";			
			
			for (int j = 0; j < arrayPixelsRe; j++) {
				
				//Skip Space for X-Axis 
				boolean skipSpace = false;
				//Get calculated Mandelbrot Value
				int mandelbrotIters = array[i][j].getMandelbrotValue();
								
				//Determine Colour to use
					//INSIDE
					if (mandelbrotIters < 0) {
						addedChar = "#";
					} 
					//OUTSIDE
					else {
						addedChar = " ";
					}
					
				//Draw the Axis
				if (drawAxis) {					
					//X-Achse
					if (Math.abs(array[i][j].getIm()) < 0.5*(1/resolution)) {
						addedChar = "~";
						skipSpace = true;
					}					
					//Y-Achse und Mittelkreuz
					if (Math.abs(array[i][j].getRe()) < 0.5*(1/resolution)) {
						addedChar = "|";
					}					
				}
				
				completeLine += addedChar;
				addedChar = "";
				
				if (skipSpace && j!=arrayPixelsRe-1) {
					completeLine += "~";
				} else {
					completeLine += " ";
				}
			}
			
			//Begrenzungsstrich rechts neben dem Bild
			completeLine += "|";
			
			//Printe die erstellte Bildlinie aus
			System.out.println(completeLine);			
			completeLine = "";
		}
		
		//Begrenzungsstrich darunter
		overline = "  ";
		for (int k = 0; k < 2*arrayPixelsRe+1; k++) {
			overline += "~";
		}
		System.out.println(overline);
	}
}
