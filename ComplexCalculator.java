
public class ComplexCalculator {
		
	//------------------------KLASSENMETHODEN------------------------
	
	public ComplexNumber add(ComplexNumber z1, ComplexNumber z2) {
		
		double re = z1.getRe() + z2.getRe();
		double im = z1.getIm() + z2.getIm();
		
		ComplexNumber result = new ComplexNumber(re, im);
		return result;
	}
	
	public ComplexNumber subtract(ComplexNumber z1, ComplexNumber z2) {
		
		double re = z1.getRe() - z2.getRe();
		double im = z1.getIm() - z2.getIm();
		
		ComplexNumber result = new ComplexNumber(re, im);
		return result;
	}
		
	public ComplexNumber multiply(ComplexNumber z1, ComplexNumber z2) {
		
		//(a + b*i) * (c + d*i) = 		
		//(ac -bd) + i(ad+bc)
		
		double re = z1.getRe() * z2.getRe() - z1.getIm() * z2.getIm();
		double im = z1.getRe() * z2.getIm() + z1.getIm() * z2.getRe();
		
		ComplexNumber result = new ComplexNumber(re, im);		
		return result;
	}
	
	public ComplexNumber divide(ComplexNumber z1, ComplexNumber z2) {
		
		//(a + b*i) / (c + d*i) =		
		//	(a + b*i)	*	(c - d*i)	ac - adi + bci + bd		(ac + bd) + i(cb - ad) 
		//	------------------------- =	-------------------	=	----------------------
		//	(c + d*i)	*	(c - d*i)	c² - cdi + cdi + d²				c² + d²
		
		double re = (z1.getRe() * z2.getRe() + z1.getIm() * z2.getIm()) / 
					(z2.getRe() * z2.getRe() + z2.getIm() * z2.getIm());
		double im = (z2.getRe() * z1.getIm() - z1.getRe() * z2.getIm()) / 
					(z2.getRe() * z2.getRe() + z2.getIm() * z2.getIm());
		
		ComplexNumber result = new ComplexNumber(re, im);		
		return result;
	}
	
	public ComplexNumber pow(ComplexNumber z1, int exponent) {		
		ComplexNumber result = z1;
		for (int i = 1; i <= exponent; i++) {
			result = multiply(result, z1);
			i++;
		}
		
		return result;
	}
	
	public double abs(ComplexNumber z1) {
		double abs = Math.sqrt(z1.getRe()*z1.getRe() + z1.getIm()*z1.getIm());
		return abs;
	}
}